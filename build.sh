#!/bin/bash

set -e

./autogen.sh || die "Autogen failed" 4
make || die "make failed" 5

if [ "$1" == "install" ]; then
  echo "Installing"
  systemctl --user stop xfce4-notifyd
  sudo cp xfce4-notifyd/xfce4-notifyd /usr/lib/x86_64-linux-gnu/xfce4/notifyd/xfce4-notifyd
  sudo cp xfce4-notifyd-config/xfce4-notifyd-config /usr/bin/xfce4-notifyd-config
  systemctl --user start xfce4-notifyd

  xfconf-query -c xfce4-notifyd -p /show-notifications-on -s choose-monitor
  xfconf-query --create --type string -c xfce4-notifyd -p /show-notifications-on-monitor -s "DisplayPort-0"
fi

echo "Done"
